= Antora Default UI
// Project URIs:
:uri-project: https://gitlab.com/antora/antora-ui-default
:uri-ci-pipelines: {uri-project}/pipelines
:img-ci-status: {uri-project}/badges/master/pipeline.svg

image:{img-ci-status}[CI Status (GitLab CI), link={uri-ci-pipelines}]

This project is an archetype that demonstrates how to produce a UI bundle for use in an Antora-based documentation pipeline.

== Copyright and License

Copyright (C) 2017 OpenDevise Inc. and the Antora Project.

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.

== Authors

Development of Antora is lead and sponsored by OpenDevise Inc.
